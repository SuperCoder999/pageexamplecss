# Page Example
A simple page with a lot of animations

### To run localy:
 - Clone the repo
 - Open index.html in latest version of Firefox or Chrome

### To deploy:
 - Fork the repo
 - Add deploy keys
 - Clone YOUR repo to the VPS
 - Run on static nginx server

# ENJOY!
